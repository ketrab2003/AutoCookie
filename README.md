# Auto Cookie Clicker
Automatically complete [Cookie Clicker Game](https://orteil.dashnet.org/cookieclicker/).

## Getting started
### Requirements
- This project requires Python v3.7.3 or later.
- You need to have downloaded corresponding version of chromedriver.

### Libraries
Install libraries from requirements.txt
```
pip install -r requirements.txt
```

### Usage
#### Running
Run program from AutoCookie/autocookie.py
```
python autocookie.py
```
For additional help type:
```
python autocookie.py -h
```

#### In-app shortcuts
- By clicking Options or Stats application will stop clicking.
  Resume it by closing Options/Stats.
- When Info is clicked applickation exits and stops running.
- When You click Export save in Options applicktion will save progress to file (default is save.txt).